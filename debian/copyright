Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Net-XMPP
Upstream-Contact: Eric Hacker <pause@erichacker.com>
Source: https://metacpan.org/release/Net-XMPP

Files: *
Copyright: 1998-2004, Jabber Software Foundation http://jabber.org/
License: LGPL-2+

Files: debian/*
Copyright: 2004-2006, Florian Ragwitz <rafl@debian.org>
 2006, Mohammed Adnène Trojette <adn+deb@diwi.org>
 2008-2014, gregor herrmann <gregoa@debian.org>
 2008, Mark Hymers <mhy@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.
