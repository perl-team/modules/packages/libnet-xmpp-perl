libnet-xmpp-perl (1.05-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Update debian/upstream/metadata: add upstream Git repo.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libxml-stream-perl.
    + libnet-xmpp-perl: Drop versioned constraint on libxml-stream-perl in
      Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:13:44 +0100

libnet-xmpp-perl (1.05-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:23:26 +0100

libnet-xmpp-perl (1.05-1) unstable; urgency=medium

  * Team upload.

  [ Axel Beckert ]
  * Add debian/upstream/metadata
  * Import upstream version 1.05
    + All patches except the disabling of network tests were applied
      upstream and hence dropped from the package.
    + Drop "Files: t/lib/Test/*" stanza from debian/copyright, the
      according code has been removed upstream.
    + Add new build-dependencies on libyaml-tiny-perl and
      liblwp-online-perl.
    + Rewrite disable_networking_tests.patch:
      - Remove those tests which now have upstream network detection
      - Add those tests which now need network detection, but haven't.
    + Restrict libxml-stream-perl (build-)dependencies to >= 1.24.
  * Recommend libdigest-hmac-perl (Closes: #794981)
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility to 9.
    + Update versioned debhelper build-dependency accordingly.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

 -- Axel Beckert <abe@debian.org>  Sun, 22 Jan 2017 23:24:56 +0100

libnet-xmpp-perl (1.02-5) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * copyright: fix path to t/lib/Test
  * Patch Net::XMPP::Stanza to load Net::XMPP::Debug before using it
    autopkgtests fail otherwise

 -- Damyan Ivanov <dmn@debian.org>  Tue, 09 Jun 2015 14:03:42 +0000

libnet-xmpp-perl (1.02-4) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * Add patch from upstream git to add DNS SRV support. (Closes: #325658)
  * Add libnet-dns-perl to Recommends (needed for DNS SRV).
  * Forward two bugs upstream, add CPAN URLs to patch headers.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Apr 2014 22:31:17 +0200

libnet-xmpp-perl (1.02-3) unstable; urgency=low

  * Team upload.
  * Add 626897_check-definedness-of-hash-key-before-use.patch to silence an
    irritating warning in current versions of Perl (closes: #626897).

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 04 Nov 2012 21:30:22 +0100

libnet-xmpp-perl (1.02-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * Remove Florian Ragwitz from Uploaders (closes: #523255).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * Update test patch to work with Perl 5.14: skip tests in "use
    Test::More" (taken from libnet-jabber-perl and Dominic's changes
    there). (Closes: #636393)
  * Switch to source format 3.0 (quilt); remove quilt framework.
  * Update patch headers.
  * Use debhelper 8 and tiny debian/rules file. Don't install README
    anymore.
  * debian/copyright: use DEP5 formatting.
  * Set Standards-Version to 3.9.2 (no changes).
  * Add patch to use Digest::SHA instead of Digest::SHA1 (cf. #594273);
    remove libdigest-sha1-perl from debian/control.
  * Add a new patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Aug 2011 20:16:43 +0200

libnet-xmpp-perl (1.02-1) unstable; urgency=low

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debian.org>); Florian Ragwitz <rafl@debian.org> moved to
    Uploaders.
  * Add debian/watch.
  * Switch patch system from dpatch to quilt.

  [ Damyan Ivanov ]
  * New upstream release

  [ gregor herrmann ]
  * debian/copyright: use author-independent upstream URL; point to correct
    file in /usr/share/common-licenses/; add copyright/license information for
    files in t/Lib/Test/*.
  * Set Standards-Version to 3.8.0; add debian/README.source to document quilt
    usage.
  * Add /me to Uploaders.
  * Refresh debian/rules, no functional changes.
  * Add libmodule-build-perl to Build-Depends.
  * Remove debian/docs, let debian/rules install the README.

 -- gregor herrmann <gregoa@debian.org>  Wed, 09 Jul 2008 19:42:00 +0200

libnet-xmpp-perl (1.0-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with Perl 5.10.  Closes: #467874

 -- Mark Hymers <mhy@debian.org>  Sat, 05 Apr 2008 21:09:09 +0100

libnet-xmpp-perl (1.0-2) unstable; urgency=medium

  * Use my @debian.org address in the Maintainer field.
  * Clean up debian/rules.
  * Use DH_COMPAT 5.
  * Build depend on dpatch.
  * Remov adn's hack which moves t/2_* and t/3_* out of the way before
    running make test and replace it with a dpatch which skips those tests
    properly.

 -- Florian Ragwitz <rafl@debian.org>  Sun,  6 Aug 2006 18:30:15 +0200

libnet-xmpp-perl (1.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Hack to disable t/2_client_jabberd1.4.t and t/3_client_jabberd2.t tests
    that try to connect the network and causes FTBFS. (Closes: #380269)
  * Bump Standards-Version to 3.7.2
     - update FSF address

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Sat,  5 Aug 2006 21:54:45 +0200

libnet-xmpp-perl (1.0-1) unstable; urgency=low

  * Initial Release (Closes: #272845).

 -- Florian Ragwitz <florian@mookooh.org>  Wed, 22 Sep 2004 13:03:02 +0200
